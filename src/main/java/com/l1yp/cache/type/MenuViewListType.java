package com.l1yp.cache.type;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l1yp.model.view.system.MenuView;

import java.util.List;

public class MenuViewListType extends TypeReference<List<MenuView>> {
}
