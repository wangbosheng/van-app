package com.l1yp.cache.type;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l1yp.model.view.system.RoleView;

import java.util.List;

public class RoleViewListType extends TypeReference<List<RoleView>> {
}
