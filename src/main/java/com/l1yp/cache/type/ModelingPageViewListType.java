package com.l1yp.cache.type;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l1yp.model.view.modeling.ModelingPageView;

import java.util.List;

public class ModelingPageViewListType extends TypeReference<List<ModelingPageView>> {
}
