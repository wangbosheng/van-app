package com.l1yp.cache.type;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l1yp.model.view.modeling.ModelingOptionTypeView;

import java.util.List;

public class ModelingOptionTypeViewListType extends TypeReference<List<ModelingOptionTypeView>> {
}
