package com.l1yp.cache.type;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l1yp.model.view.modeling.ModelingOptionValueView;

import java.util.List;

public class ModelingOptionValueViewListType extends TypeReference<List<ModelingOptionValueView>> {
}
