package com.l1yp.cache.type;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l1yp.model.db.modeling.ModelingField;

import java.util.List;

public class ModelingFieldListType extends TypeReference<List<ModelingField>> {
}
