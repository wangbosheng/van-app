package com.l1yp.cache.type;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l1yp.model.db.system.Department;

import java.util.List;

public class DepartmentListType extends TypeReference<List<Department>> {
}
