package com.l1yp.cache.type;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l1yp.model.view.modeling.ModelingViewSimpleInfo;

import java.util.List;

public class ModelingViewSimpleInfoListType extends TypeReference<List<ModelingViewSimpleInfo>> {
}
