package com.l1yp.cache.type;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l1yp.model.common.PageData;
import com.l1yp.model.view.modeling.ModelingEntityView;

public class ModelingEntityViewPageType extends TypeReference<PageData<ModelingEntityView>> {
}
