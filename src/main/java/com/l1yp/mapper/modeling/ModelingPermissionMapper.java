package com.l1yp.mapper.modeling;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.l1yp.model.db.modeling.ModelingPermission;

public interface ModelingPermissionMapper extends BaseMapper<ModelingPermission> {
}
