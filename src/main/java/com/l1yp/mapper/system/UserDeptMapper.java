package com.l1yp.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.l1yp.model.db.system.UserDept;

public interface UserDeptMapper extends BaseMapper<UserDept> {
}
