package com.l1yp.service.system.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.l1yp.mapper.system.UserDeptMapper;
import com.l1yp.model.db.system.UserDept;
import org.springframework.stereotype.Service;

@Service
public class UserDeptServiceImpl extends ServiceImpl<UserDeptMapper, UserDept> {
}
